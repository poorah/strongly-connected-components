#include <bits/stdc++.h>

using namespace std ;

class Graph {
    public:
        // properties :
        map<int, bool> visited;
        map<int, list<int>> adj;
        map<int, list<int>> reverseAdj;
        map<int, int> preNum;
        map<int, int> postNum;
        stack<int> pst;
        int clock = 1;
        //methods :
        void addEdge(int v, int w);
        void Explore(int v);
        void preVisit(int v);
        void postVisit(int v);
        void RDFS();
        void ExploreR(int v);
        void SCCs();
        void makeAllFalse();
};

void Graph::addEdge(int v, int w) {
    //TODO
    adj[v].push_back(w);
    reverseAdj[w].push_back(v);
}

void Graph::makeAllFalse() {
    //TODO
    map<int, bool>::iterator i;

    for(i = visited.begin(); i != visited.end(); i++) {
        visited[i->first] = false ;
    }
}

void Graph::SCCs() {
    //TODO
    RDFS();
    makeAllFalse();
    while(!pst.empty()) {
        if(!visited[pst.top()]) {
            Explore(pst.top());
            cout << "end\n";
        }
        pst.pop();
    }
}

void Graph::Explore(int v) {
    //TODO
    visited[v] = true ;
    preVisit(v);
    cout << v << " -> " ;
    list<int>::iterator i;

    for(i = adj[v].begin(); i != adj[v].end(); i++) {
        if(!(visited[*i])) {
            Explore(*i) ;
        }
    }
    
    postVisit(v);
}

void Graph::ExploreR(int v) {
    //TODO
    visited[v] = true ;
    preVisit(v);
    list<int>::iterator i;

    for(i = reverseAdj[v].begin(); i != reverseAdj[v].end(); i++) {
        if(!(visited[*i])) {
            ExploreR(*i) ;
        }
    }
    
    postVisit(v);
}

void Graph::preVisit(int v) {
    preNum[v] = clock;
    clock++;
}

void Graph::postVisit(int v) {
    postNum[v] = clock;
    clock++;
    pst.push(v);
}

void Graph::RDFS() {

    map<int, list<int>>::iterator i;
    
    for(i = reverseAdj.begin(); i != reverseAdj.end(); i++) {
        if(!(visited[i->first])) {
            ExploreR(i->first) ;
        }
    }
}



int main() {

    Graph g ;
    
    
    g.addEdge(1, 2);
    g.addEdge(2, 4);
    g.addEdge(2, 5);
    g.addEdge(3, 2);
    g.addEdge(4, 3);
    g.addEdge(4, 1);
    g.addEdge(4, 7);
    g.addEdge(6, 5);
    g.addEdge(6, 7);
    g.addEdge(7, 6);
    g.addEdge(8, 7);
    g.addEdge(9, 8);
    g.addEdge(9, 1);
    g.SCCs();
    
    return 0;
}
